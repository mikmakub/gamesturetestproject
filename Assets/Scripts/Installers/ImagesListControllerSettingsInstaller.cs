using UnityEngine;
using Zenject;

namespace MKubiak.ImagesList.Zenject
{
    [CreateAssetMenu(fileName = "ImagesListControllerSettingsInstaller", menuName = "Installers/ImagesListControllerSettingsInstaller")]
    public class ImagesListControllerSettingsInstaller : ScriptableObjectInstaller<ImagesListControllerSettingsInstaller>
    {
        [SerializeField] private ImagesListController.Settings imagesListControllerSettings;

        public override void InstallBindings()
        {
            Container.Bind<ImagesListController.Settings>().FromInstance(imagesListControllerSettings).AsSingle();
        }
    }
}