using UnityEngine;
using Zenject;

namespace MKubiak.ImagesList.Zenject
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private GameObject imageElemetFacadePrefab;

        public override void InstallBindings()
        {
            Container.BindFactory<ImageElementFacade, ImageElementFacade.Factory>()
                .FromPoolableMemoryPool<ImageElementFacade, ImageElementFacadePool>(poolBinder => poolBinder
                .WithInitialSize(50)
                .FromComponentInNewPrefab(imageElemetFacadePrefab)
                .UnderTransformGroup("ImageElementsPool"));
        }
    }
}