using UnityEngine;
using Zenject;

namespace MKubiak.ImagesList.Zenject
{
    public class ListPanelInstaller : MonoInstaller
    {
        [SerializeField] private Transform listParent;

        public override void InstallBindings()
        {
            Container.BindInstance(listParent).AsSingle();
            Container.Bind<ImagesListController>().AsSingle();
        }
    }
}