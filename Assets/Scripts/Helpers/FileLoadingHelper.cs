using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace MKubiak.ImagesList.Helpers
{
    public class FileLoadingHelper : MonoBehaviour
    {
        private static List<string> filesFound = new List<string>();

        public static string[] GetFilesFrom(string searchFolder, string[] filters, bool isRecursive)
        {
            filesFound.Clear();
            SearchOption searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, string.Format("*.{0}", filter), searchOption));
            }
            return filesFound.ToArray();
        }

        public static async Task<byte[]> ReadAllBytesAsync(string path)
        {
            using (var fileStream = File.OpenRead(path))
            {
                var buffer = new byte[fileStream.Length];
                await fileStream.ReadAsync(buffer, 0, (int)fileStream.Length);
                return buffer;
            }
        }
    }
}
