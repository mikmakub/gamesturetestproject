﻿using UnityEngine;
using Zenject;

namespace MKubiak.ImagesList
{
    public class PoolableFacade : MonoBehaviour, IPoolable<IMemoryPool>
    {
        protected IMemoryPool pool;

        public void OnDespawned()
        {

        }

        public void OnSpawned(IMemoryPool pool)
        {
            this.pool = pool;
        }

        public void Despawn()
        {
            pool.Despawn(this);
        }
    }
}
