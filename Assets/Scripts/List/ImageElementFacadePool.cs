﻿using Zenject;

namespace MKubiak.ImagesList
{
    public class ImageElementFacadePool : MonoPoolableMemoryPool<IMemoryPool, ImageElementFacade>
    {
    }
}
