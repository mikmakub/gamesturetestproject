﻿using MKubiak.ImagesList.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using Directory = System.IO.Directory;
using File = System.IO.File;

namespace MKubiak.ImagesList
{
    public class ImagesListController
    {
        private Settings settings;
        private ImageElementFacade.Factory imageElemetsFactory;
        private Transform listParent;

        private readonly string[] imagesFilters = new string[] { "png" };
        private List<Texture2D> textures = new List<Texture2D>();
        private List<ImageElementFacade> listElements = new List<ImageElementFacade>();
        private bool isSettingAList;

        public ImagesListController(Settings settings, ImageElementFacade.Factory imageElemetsFactory, Transform listParent)
        {
            this.settings = settings;
            this.imageElemetsFactory = imageElemetsFactory;
            this.listParent = listParent;
        }

        public async void SetupList(Action onListSetupAction = null)
        {
            if (isSettingAList)
            {
                return;
            }

            isSettingAList = true;

            string[] imagesPaths = GetImagesPaths();
            ClearList();
            await PopulateList(imagesPaths);
            onListSetupAction?.Invoke();
            isSettingAList = false;
        }

        private string[] GetImagesPaths()
        {
            string[] imagesPaths = new string[0];

            if (Directory.Exists(settings.imagesDirectoryPath))
            {
                imagesPaths = FileLoadingHelper.GetFilesFrom(settings.imagesDirectoryPath, imagesFilters, true);
            }
            else
            {
                Debug.LogError("No such directory: \"" + settings.imagesDirectoryPath + "\" on your machine. Please enter proper directory path to Assets/Settings/ImagesListControllerSettingsInstaller settings file");
            }

            return imagesPaths;
        }

        private void ClearList()
        {
            foreach (var element in listElements)
            {
                element.Despawn();
            }

            listElements.Clear();
        }

        private async Task PopulateList(string[] imagesPaths)
        {
            foreach (var imagePath in imagesPaths)
            {
                Texture2D texture = await GetTextureAsync(imagePath);
                ImageElementData imageElementData = GetImageElementData(imagePath, texture);

                CreateImageListElement(imageElementData);
            }
        }

        private async Task<Texture2D> GetTextureAsync(string imagePath)
        {
            byte[] imageFileData = await FileLoadingHelper.ReadAllBytesAsync(imagePath);
            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(imageFileData);
            textures.Add(texture);
            return texture;
        }
        private static ImageElementData GetImageElementData(string imagePath, Texture2D texture)
        {
            ImageElementData imageElementData = new ImageElementData();
            imageElementData.imageName = Path.GetFileName(imagePath);
            imageElementData.imageDate = DateTime.Now.Subtract(File.GetCreationTime(imagePath)).ToString(@"dd\:hh\:mm\:ss");
            imageElementData.imageTexture = texture;
            return imageElementData;
        }

        private void CreateImageListElement(ImageElementData imageElementData)
        {
            var imageListElement = imageElemetsFactory.Create();
            imageListElement.transform.SetParent(listParent);
            imageListElement.transform.localScale = new Vector3(1f, 1f, 1f);
            imageListElement.SetElement(imageElementData);
            listElements.Add(imageListElement);
        }

        [Serializable]
        public class Settings
        {
            public string imagesDirectoryPath;
        }
    }
}
