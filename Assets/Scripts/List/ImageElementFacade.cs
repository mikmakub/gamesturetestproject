﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace MKubiak.ImagesList
{
    public class ImageElementFacade : PoolableFacade
    {
        [SerializeField] private TextMeshProUGUI imageNameText;
        [SerializeField] private TextMeshProUGUI imageDateText;
        [SerializeField] private RawImage image;

        public void SetElement(ImageElementData imageElementData)
        {
            imageNameText.text = imageElementData.imageName;
            imageDateText.text = imageElementData.imageDate;
            image.texture = imageElementData.imageTexture;
        }

        public class Factory : PlaceholderFactory<ImageElementFacade>
        {
        }
    }
}
