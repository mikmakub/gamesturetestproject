using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace MKubiak.ImagesList
{
    public class RefreshButtonEventsHandler : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Button refreshButton;
        private ImagesListController imagesListController;

        [Inject]
        private void Contruct(ImagesListController imagesListController)
        {
            this.imagesListController = imagesListController;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            imagesListController.SetupList(UnlockButton);
            LockButton();
        }

        private void LockButton()
        {
            refreshButton.interactable = false;
        }

        private void UnlockButton()
        {
            refreshButton.interactable = true;
        }
    }
}
