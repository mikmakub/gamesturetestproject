﻿using UnityEngine;

namespace MKubiak.ImagesList
{
    public struct ImageElementData
    {
        public string imageName;
        public string imageDate;
        public Texture2D imageTexture;
    }
}
